notes.js
/*

    Separation of Concerns
    Models Folder
    - Contain the Object Schemas and defines the object structure and content

    Controllers Folder
    - Contain the functions and business logic of our Express JS application
    - Meaning all the operations it can do will be placed in this file

    Routes Folder
    - Contains all the endpoints for our application
    - We separate the routes such that "index.js" only contains information on the server
    
    require -> to include a specific module/package.
    export -> to treat a value as a "module" that can be used by other files

    Flow of exports and require: 
    export models > require in controllers 
    export controllers > require in routes
    export routes > require in app.js


*/